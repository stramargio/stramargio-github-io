---
layout: post
title:  "Restart"
date:   2022-12-12 08:30:00
tags: [general]
---

After a few years I felt the need to rebuild my space away from centralized social media, where I could write about both technical and personal topics.

On the blog you will find all the thoughts that don't fit into a toot (on the [fediverse](https://margio.de/@andrea)).

👋🏻
